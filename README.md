# Markdown 2 PDF

This is my preferred setup to convert Markdown documents into PDF documents.
Other file formats such as EPUB are not supported since the Eisvogel tamplate doesn't support it.

## Usage

To use it locally, clone the repository recursively, build the docker image and use it.
All non-hidden .md files in the exposed directory will be handled by converted.

```shell
git clone --recurse-submodules https://gitlab.com/Slyso/markdown-2-pdf.git
./pandoc/build.sh
docker run --volume $(pwd)/docs:/data pandoc-eisvogel
```

## Demo

An example Markdown file with the resulting PDF output can be found in the [docs](docs/) directory.

## GitLab pipeline

```yaml
stages:
  - build
  - run

build:
  stage: build
  when: manual
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - docker pull $CI_REGISTRY_IMAGE || true
    - apk add git
  script:
    - test -d markdown-2-pdf || git clone --recurse-submodules https://gitlab.com/Slyso/markdown-2-pdf.git
    - cd markdown-2-pdf && git pull origin master
    - cd pandoc && docker build --cache-from $CI_REGISTRY_IMAGE --tag $CI_REGISTRY_IMAGE .
    - docker push $CI_REGISTRY_IMAGE
  cache:
    paths:
      - markdown-2-pdf

generate-pdf:
  stage: run
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker run --volume $(pwd):/data $CI_REGISTRY_IMAGE
  artifacts:
    paths:
      - doc.pdf
```
