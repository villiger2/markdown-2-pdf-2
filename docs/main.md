---
title: "Example"
author: Thomas Zahner
date: 1. January 2000
titlepage: true

# table of contents
toc: true
toc-own-page: true
toc-depth: 2

#mainfont: DejaVuSerif
fontsize: 10pt
colorlinks: true
link-citations: true

# Control the margins of the document
geometry: "left=2cm,right=2cm,top=2cm,bottom=2.5cm"

# Language of generated names in the document, such as "table", "figure", "list of figures", etc.
lang: en
...

# Intro

- Using Docker we don't have to install any dependencies manually
- The [Eisvogel](https://github.com/Wandmalfarbe/pandoc-latex-template/tree/v2.0.0) template for beautiful PDF
  generation and customization.
- [PlantUML](https://plantuml.com/) to render UML like diagrams

# YAML headers

[YAML headers](https://pandoc.org/MANUAL.html#extension-yaml_metadata_block) are a really powerful and useful way to
control how documents are converted. You can for example define a title page, properties for the table of contents and
more.

Here is an example of the YAML header used for this document:

```yaml
---
title: "Example"
author: Thomas Zahner
date: 1. January 2000
titlepage: true

# table of contents
toc: true
toc-own-page: true
toc-depth: 2

#mainfont: DejaVuSerif
fontsize: 10pt
colorlinks: true
link-citations: true

# Control the margins of the document
geometry: "left=2cm,right=2cm,top=2cm,bottom=2.5cm"

# Language of generated names in the document, such as "table", "figure", "list of figures", etc.
lang: en
...
```

# Plant UML

Plant UML rendering is also supported.

```plantuml
@startuml BobAndAlice
Alice -> Bob: Authentication Request
Bob --> Alice: Authentication Response
@enduml
```
![Alice communicating with Bob](plantuml-images/BobAndAlice.png)

# Other useful information

## Referencing Markdown paragraphs

If you have ever wondered how to create links to other Markdown paragraphs here is the answer.
It's actually really simple. Just create a link how you would normally but instead of entering a URL in the brackets
enter a single hashtag followed by the paragraph name. Just make sure to replace uppercase with lowercase letters
and spaces with hyphens. The cool thing is that in the exported PDF you can just click on the links
to jump to the paragraph if your PDF viewer supports the feature. Here are some examples:

[_Link to the intro section_](#intro)

```markdown
[Link to the intro section](#intro)

[_Another link_](#referencing-markdown-paragraphs)
```

## Image sizing

Pandoc makes resizing images really simple. Just add additional properties in curly braces after the normal Markdown image link:

```markdown
Image with 20% width:

![](media/markdown-logo.svg){ width=20% }
```

Image with 20% width:

![](media/markdown-logo.svg){ width=20% }

```markdown
Image with a height of 30px:

![](media/markdown-logo.svg){ height=30px }
```

Image with a height of 30px:

![](media/markdown-logo.svg){ height=30px }

## Page and line breaks

Since LaTeX commands are allowed we can easily create page and line breaks like so:

```markdown
\newpage
\newline
```

For more information see [LaTeX Line and Page Breaking](http://www.personal.ceu.hu/tex/breaking.htm).

## Change font

### Font type

\textrm{Serif Font Family}

\textsf{Sans Serif Font Family}

\texttt{Monospaced Font Family}

\rmfamily
Serif Font Family

\sffamily
Sans Serif Font Family

\ttfamily
Monospaced Font Family
\sffamily

```markdown
\textrm{Serif Font Family}

\textsf{Sans Serif Font Family}

\texttt{Monospaced Font Family}

\rmfamily
Serif Font Family

\sffamily
Sans Serif Font Family

\ttfamily
Monospaced Font Family
\sffamily
```

### Font size

\begin{huge}
Huge
\end{huge}

\scriptsize
Small text.
\normalsize

```markdown
\begin{huge}
Huge
\end{huge}

\scriptsize
Small text.
\normalsize
```


## Mathematical equations

Prefix and postfix your LaTeX formulas with the `$` sign:

$E=mc^2$

```markdown
$E=mc^2$
```

## Include PDFs

It's possible to include PDFs like including images. The only problem is that currently only the first page of the PDF is included.

```markdown
![](media/myPdf.pdf)
```

If you want to include multiple pages: import pdfpages in the YAML header and use includepdf:

```markdown
---
header-includes:
  - \usepackage{pdfpages}
...

\includepdf[pages=-]{media/myPdf.pdf}
```

## Align images siede-by-side

Luckily it's possible to display images side-by-side. (source: https://stackoverflow.com/a/51490189)

![](media/markdown-logo.svg){width=60%}
![](media/markdown-logo.svg){width=40%}
\begin{figure}[!h] \caption{A single caption for the two subfigures} \end{figure}

```markdown
![](media/markdown-logo.svg){width=60%}
![](media/markdown-logo.svg){width=40%}
\begin{figure}[!h] \caption{A single caption for the two subfigures} \end{figure}
```

## Citations

Zur Elektrodynamik bewegter Körper. [@einstein]

## List of figures

To display the list of figures use the following LaTeX command:

```markdown
\listoffigures
```

\listoffigures

## Code blocks with labels & captions

```markdown
~~~~{.typescript caption="The caption" label=foo}
foo();
~~~~

In listing \ref{foo} the function `foo` is invoked.
```

~~~~{.typescript caption="The caption" label=foo}
foo();
~~~~

In listing \ref{foo} the function `foo` is invoked.

# References

Here are my references:

<div id="refs"></div>
